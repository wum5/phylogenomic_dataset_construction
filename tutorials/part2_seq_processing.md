#Part 2: Sequence processing, assembly, translation and baited homology search

In general, we don't say at each step that you should look at the input and output, but please take some time to see what files are used for input and what are produced in output.

##Get some basic statistics from the fastq files 
cd in to the directory examples/seqcore_pe_stranded.

	cd ~/phylogenomic_dataset_construction/examples/seq_processing/seqcore_pe_stranded
	
The two .gz files contain read pairs from a stranded mRNA library of _Drosera binata_ prepared using the KAPA stranded RNA-seq kit with poly-A enrichment. This sample data set is about 1/20 of a full data set. These are the original files we got from the University of Michigan sequencing core. You can see the first 10 lines of the file on linux by

	zcat 41624_CTTGTA_L005_R1_010.fastq.gz | head

To count how many lines there are in the file

	zcat 41624_CTTGTA_L005_R1_010.fastq.gz | wc -l
	
Divide the line numbers by 4 you get the number of reads. On mac use gunzip -c instead of zcat:

	gunzip -c 41624_CTTGTA_L005_R1_010.fastq.gz | head
	gunzip -c 41624_CTTGTA_L005_R1_010.fastq.gz | wc -l

You can print out a subset of quality scores. For all the python scripts in this repository, by typing the python command without arguments, they will print out what arguments you need to provide. You can also open the python file to see additional instructions on input file format etc. The following script will give you an error message if the phred score offset is out of the range. If you get the warning, change the phred score offset in seq.py from 33 to 64 for older Illumina reads.

	python ~/phylogenomic_dataset_construction/print_quality_scores.py 41624_CTTGTA_L005_R1_010.fastq.gz
	
Visualize quality score distribution in R

	R
	a=read.table('41624_CTTGTA_L005_R1_010.fastq.gz.qual')
	boxplot(a)

Quit R after you finish

	q()
	n

##Trim adapters
For sra data sets, use the fastq-dump tool from the [ sra toolkit ](http://www.ncbi.nlm.nih.gov/books/NBK158900/) to extract fastq files, and the script filter\_fastq.py to remove adapters since both the adapter sequences and phred scores are unknown. For data sets with known adapters I use Trimmomatic as part of the Trinity pipeline for adapter trimming instead. Trimmomatic is much faster but would not give an error message if the adapter or phred score offset were misspedified. We are not analyzing sra data sets for the tutorials but see the README.md file for the phylogenomic_dataset_construction repository for additional instruction.


##*De novo* assembly with Trinity
Choose a taxonID for each data set. This taxonID will be used throughout the analysis. Use short taxonIDs with 4-6 letters and digits with no special characters. Here we use Dbin as the taxonID, which is short for _Drosera binata_.

A typical Trinity call for analyzing Smith Lab sequences (custom adapter file, trimmomatic for both adapter clipping and quality trimming, stranded) looks like:

	ulimit -s unlimited
	Trinity --seqType fq --trimmomatic --quality_trimming_params "ILLUMINACLIP:/home/yayang/optimize_assembler/data/TruSeq_adapters:2:30:10 SLIDINGWINDOW:4:5 LEADING:5 TRAILING:5 MINLEN:25" --max_memory 100G --CPU 9 --full_cleanup --SS_lib_type RF --output taxonID.trinity --left <forward reads> --right <reverse reads>

A typical Trinity call for analyzing paired-end sra sequences (start from fastq files with adapters removed, use trimmomatic only for quality trimming, non-stranded reads)

	ulimit -s unlimited
	Trinity --seqType fq --trimmomatic --quality_trimming_params "SLIDINGWINDOW:4:5 LEADING:5 TRAILING:5 MINLEN:25" --max_memory 100G --CPU 9 --full_cleanup --output taxonID.trinity --left <forward reads with adatper removed> --right <reverse reads with adapter removed>

For tutorial we don't run Trinity since installing Trinity can be tricky and you don't use a laptop to run Trinity anyways after you go through all the trouble to install it. A Trinity output file Dbin.Trinity.fasta is provided for the data set seqcore_pe_stranded.
	
##Translation using TransDecoder with blastp for choosing orfs

TransDecoder provides two options for choosing the orfs to retain after scoring candidate orfs. I benchmarked blastp vs. PfamAB vs. both and found that since we have closely related, high quality proteomes available for the Caryophyllales, blastp using a custom blast database is much faster (hours vs. days) and more sensitive than Pfam. You should try both options and see the difference for your data sets. Here we use the blastp-only option.

Create a custom blast database using closely-related, high quality proteomes from _Arabidopsis thaliana_ and _Beta vulgaris_:

	cd ~/phylogenomic_dataset_construction/data/blastp_database
	cat *.fa >db
	makeblastdb -in ./db -parse_seqids -dbtype prot -out db

cd into the directory where the Trinity output file is

	cd ~/phylogenomic_dataset_construction/examples/seq_processing/seqcore_pe_stranded
	
Find candidate orfs (-S for stranded. Remove -S for non-stranded data sets):

	~/apps/TransDecoder/TransDecoder.LongOrfs -t Dbin.Trinity.fasta -S
	
Blast all candidate orfs against reference we created

	blastp -query Dbin.Trinity.fasta.transdecoder_dir/longest_orfs.pep -db ~/phylogenomic_dataset_construction/data/blastp_database/db -max_target_seqs 1 -outfmt 6 -evalue 10 -num_threads 2 > taxonID.blastp.outfmt6
	
Output the final orfs preferentially retaining the orfs with blast hits

	~/apps/TransDecoder/TransDecoder.Predict -t Dbin.Trinity.fasta --retain_blastp_hits taxonID.blastp.outfmt6 --cpu 2
	
The blastp step takes about 10 minutes to finish with two cores. In the mean time you can open the unfinished result file taxonID.blastp.outfmt6. The top blast hit is usually Beta vulgaris, the species more closely-related to _Drosera_. Occasionally the top hit is _Arabidopsis thaliana_, which is slightly more distantly related but better annotated. The blastp step is only for picking ORF so as long as an ORF hits something it will be retained in the final results. See the [ TransDecoder documentation ](https://transdecoder.github.io/#incl_homology) for more information.

##Shorten sequence ids
The TransDecoder output files are taxonID.Trinity.fasta.transdecoder.pep and taxonID.Trinity.fasta.transdecoder.cds. Shorten the sequence namess to taxonID@seqID. The special character "@" is used to separate taxonID and seqID.

	python ~/phylogenomic_dataset_construction/fix_names_from_transdecoder.py . .
	
The output are taxonID.pep.fa and taxonID.cds.fa. You can modify the script fix_names_from_transdecoder.py if you want to instead retain the subcomponent and isoform information output by Trinity.

The input sequences for homology inference can be cds or peptides depending on how closely-related the focal taxa are. We provided a small data set of five taxa in the directory ~/phylogenomic\_dataset\_construction/examples/homology\_inference. Because these five species are fairly closely-related (< 50 my), using cds for mcl can avoid creating large clusters without loosing sequences. For baited homology search, however, we usually use peptide sequences for its higher sensitivity with divergent sequences.

Before homology search, it always worth spending some time to make sure that the sequence names are formated correctly and peptides and cds have matching sequence names. Check for duplicated names, special characters other than digits, letters and "_", all names follow the format taxonID@seqID, and file names are the taxonID. It's good to check especially when some of the data sets were obtained from elsewhere. Most peptides and CDS files from genome annotation contain long names, spaces and special characters and should be eliminated before clustering.

	cd phylogenomic_dataset_construction/examples/homology_inference/data
	python ~/phylogenomic_dataset_construction/check_names.py . .fa

##Baited homology search using swipe
The baits we will be using is the fasta file UFGT.pep.fa. It contains flavonoid-3-O-glucosyltransferase from Arabidopsis thaliana. This gene codes for an enzyme involved in anthocyanin biosynthesis and plant coloration. Arabidopsis has four copies of UFGT due to two rounds of genome duplications. The script will use these 4 baits to search each of the 5 Caryophyllales taxon and take the top 10 hits per taxon. You will need to have blast+ 2.2.30. If you see something flash by that says that there is an error parsing the sequence id, then you have the older blast+.

	cd ~/phylogenomic_dataset_construction/examples/bait
	python ~/phylogenomic_dataset_construction/bait_homologs.py UFGT.pep.fa data 10 2

For each taxon, the script output the blastp and swipe results. The swipe output columns are

	Query id, Subject id, % identity, alignment length, mismatches, gap openings, q. start, q. end, s. start, s. end, e-value, bit score
	
The blastp output columns are

	qseqid qlen sseqid slen frames pident nident length mismatch gapopen qstart qend sstart send evalue bitscore
	
For each blastp/swipe outputs, the scripts sort the hits from all 4 bait sequences by bitscore from high to low, and write the top 10 subject ids in the .hits file. Open the .hits files and compare the ranking between blastp.hits vs. swipe.hits. For example

	head data/SCAO*hits

The rank by swipe vs. blastp are slightly different. The scripts also output a file UFGT\_blastp.fa with all the top ranked sequences plus baits, and a file UFGT\_swipe.fa with the top swipe hits plus baits. Make an alignment and a tree from each fasta file
	
	python ~/phylogenomic_dataset_construction/fasta_to_tree.py ~/phylogenomic_dataset_construction/examples/bait 2 aa n
	
You can visualize the alignments and trees. You can further extract subclades containing the baits to refine alignment etc but we'll stop here for tutorial.

	
